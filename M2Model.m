%Generating a state space model for X-tip based on the Entire Experiments
clear all
%load data set experiment 1
load('Data_Total.mat')
Ts = 1/sample_f;
Arcs = 206264.8062471;

inp_X1 = data1(:,3)*Arcs; % x-Tip: static position holding
out_X1 = data1(:,2)*Arcs;

sys_X1 = iddata(out_X1,inp_X1,Ts); %encapsulate inp/out for the sys we want to identify

%% Experiment 2
inp_X2 = data2(:,3)*Arcs;
out_X2 = data2(:,2)*Arcs;

sys_X2 = iddata(out_X2,inp_X2,Ts);

%% Experiment 3
inp_X3 = data3(:,3)*Arcs;
out_X3 = data3(:,2)*Arcs;

sys_X3 = iddata(out_X3,inp_X3,Ts);

%% Experiment 4

inp_X4 = data4(:,3)*Arcs;
out_X4 = data4(:,2)*Arcs;

sys_X4 = iddata(out_X4,inp_X4,Ts);

%% Experiment 5
inp_X5 = data5(:,3)*Arcs;
out_X5 = data5(:,2)*Arcs;

sys_X5 = iddata(out_X5,inp_X5,Ts);

%% Experiment 6
inp_X6 = data6(:,3)*Arcs;
out_X6 = data6(:,2)*Arcs;

sys_X6 = iddata(out_X6,inp_X6,Ts);


%% Merging all the experiments

%Total_data = merge(sys_X1,sys_X2,sys_X3,sys_X4,sys_X5,sys_X6);
Smerge = merge(sys_X1,sys_X2,sys_X3,sys_X4);


%% Fitting model to data
%ss_X = ssest(sys,4, 'DisturbanceModel', 'none'); %state space not working
Tf = tfest(Smerge,4,2,0);


%% plots
subplot(3,1,1), compare(sys_X6, Tf); %verify model
subplot(3,1,2), compare(sys_X2, Tf); %verify model
subplot(3,1,3), compare(sys_X1, Tf); %verify model


%%


num = [4773 1.133e06 2.345e05];
den = [1 159.1 2.439e04 1.44e06 2.345e05];
G1 = tf(num,den);


% num = [5773 1.133e06 2.345e05];
% den = [1 139.1 2.439e04 1.44e06 2.345e05];

% num = [5773 1.133e06 2.345e05];
% den = [1 139.1 2.439e04 1.44e06 2.345e05];

den1 = 1e4*[0.0001 0.006550973143863 1.955713075861281];   % figure 4, K=1.29
den2 = [1 73.590268561366116 11.990511435156604];

wn = sqrt(den1(3));
wn = wn+19;
zeta = den1(2)/(2*wn);
den1n = [1 2*wn*zeta wn^2];

G2 = tf(1.29*num,conv(den1n,den2));
%G = tf(num,den);

%%

figure; 
Ts = 1/sample_f;
T = length(inp_X4)*Ts;
tspan = linspace(0,T,length(inp_X4));

subplot(3,2,1);
hold on;
grid on;
box on;
y=lsim(G1,inp_X4,tspan);
plot(tspan,out_X4,'Color',[0.6,0.6,0.6]);
plot(tspan,y,'b');
subplot(3,2,2);
hold on;
grid on;
box on;
y=lsim(G2,inp_X4,tspan);
plot(tspan,out_X4,'Color',[0.6,0.6,0.6]);
plot(tspan,y,'b');


T = length(inp_X5)*Ts;
tspan = linspace(0,T,length(inp_X5));
subplot(3,2,3);
hold on;
grid on;
box on;
y=lsim(G1,inp_X5,tspan);
plot(tspan,out_X5,'Color',[0.6,0.6,0.6]);
plot(tspan,y,'b');subplot(3,2,4);
subplot(3,2,4);
hold on;
grid on;
box on;
y=lsim(G2,inp_X5,tspan);
plot(tspan,out_X5,'Color',[0.6,0.6,0.6]);
plot(tspan,y,'b');


T = length(inp_X6)*Ts;
tspan = linspace(0,T,length(inp_X6));
subplot(3,2,5);
hold on;
grid on;
box on;
y=lsim(G1,inp_X6,tspan);
plot(tspan,out_X6,'Color',[0.6,0.6,0.6]);
plot(tspan,y,'b');subplot(3,2,4);
subplot(3,2,6);
hold on;
grid on;
box on;
y=lsim(G2,inp_X6,tspan);
plot(tspan,out_X6,'Color',[0.6,0.6,0.6]);
plot(tspan,y,'b');

set(findall(gcf,'type','line'),'linewidth',3);

% xlim([10.15 10.4])
% ylim([-72 -96])

%% Try 2: Cropped data

%I_X1 = [inp_X1(1,1)*ones(1100,1); 3.6603e-6*ones(1401,1)]; % x-Tip: static position holding
%O_X1 = data1(36000:38500,2);
%S1 = iddata(O_X1,I_X1,Ts);

%I_X2 = [inp_X2(1,1)*ones(1297,1); 3.6677e-5*ones(704,1)]; % x-Tip: static position holding
%O_X2 = data2(37000:39000,2);
%S2 = iddata(O_X2,I_X2,Ts);

%I_X3 = [inp_X3(1,1)*ones(702,1); 38349e-4*ones(599,1)]; % x-Tip: static position holding
%O_X3 = data3(38000:39300,2);
%S3 = iddata(O_X3,I_X3,Ts);

%I_X6 = [inp_X6(1,1)*ones(702,1); 3.8721e-4*ones(599,1)]; % x-Tip: static position holding
%O_X6 = data3(37500:39000,2);
%S6 = iddata(O_X6,I_X6,Ts);

%Smerge = merge(S1,S3);

